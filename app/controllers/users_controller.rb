class UsersController < ApplicationController

  def index
    @current = self.current_user if self.current_user
    @users = User.all
    render :index
  end

  def new
    session[:session] = nil
    @user = User.new
    render :new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login(@user)
      redirect_to user_url(@user.id)
    else
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
    render :show
  end


  private
  def user_params
    params[:user].permit(:email, :password)
  end

end
