class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception

  helper_method :current_user
  helper_method :logged_in?


  def login(user)
    user.reset_token
    user.save!
    session[:session] = user.session_token
  end

  def current_user
    return nil if session[:session] == nil
    @current_user ||= User.find_by(session_token: session[:session])
  end

  def logged_in?
    !!current_user
  end

end
