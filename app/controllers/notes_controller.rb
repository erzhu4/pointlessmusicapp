class NotesController < ApplicationController


  def create
    @track_id = params[:track_id]
    @note = Note.new(title: params[:note][:title], body: params[:note][:body],
                        track_id: @track_id, user_id: current_user.id)
    @note.save!
    redirect_to track_url(@track_id)

  end

  def destroy
    @note = Note.find_by(id: params[:id])
    track_id = @note.track.id
    @note.destroy if @note
    redirect_to track_url(track_id)
  end

end
