class TracksController < ApplicationController

  before_action do
    redirect_to new_session_url if !logged_in?
  end

  def show
    @user = self.current_user
    @track = Track.find_by(id: params[:id])
    render :show
  end

  def edit
    @track = Track.find_by(id: params[:id])
    render :edit
  end

  def update
    @track = Track.find_by(id: params[:id])
    @track.update!(lyrics: params[:track][:lyrics])
    redirect_to track_url(params[:id])
  end

  def new
    @albums = Album.all
    render :new
  end

  def create
    @track = Track.new(track_params)
    if @track.save
      redirect_to album_url(params[:track][:album_id])
    else
      render :new
    end
  end

  private
  def track_params
    params[:track].permit(:song_name, :album_id)
  end

end
