class AlbumsController < ApplicationController

  before_action do
    redirect_to new_session_url if !logged_in?
  end

  def show
    @album = Album.find(params[:id])
    render :show
  end

  def new
    @bands = Band.all
    render :new
  end

  def create
    @album = Album.new(album_params)
    if @album.save!
      redirect_to band_url(params[:album][:band_id])
    else
      render :new
    end
  end

  private
  def album_params
    params[:album].permit(:title, :band_id)
  end

end
