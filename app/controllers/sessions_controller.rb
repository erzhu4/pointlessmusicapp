class SessionsController < ApplicationController

  def new
    @msg = ""
    render :new
  end

  def create
    @user = User.find_by_credentials(params[:session][:email], params[:session][:password])
    if @user
      login(@user)
      redirect_to user_url(@user.id)
    else
      @msg = "The log in information given was wrong!!"
      render :new
    end
  end

  def destroy
    user = User.find_by(session_token: session[:session])
    session[:session] = nil
    if user
      user.session_token = nil
      user.save!
    end
    redirect_to users_url
  end

end
