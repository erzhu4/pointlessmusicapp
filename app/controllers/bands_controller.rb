class BandsController < ApplicationController

  before_action do
    redirect_to new_session_url if !self.logged_in?
  end


  def index
    @user = self.current_user if current_user
    @bands = Band.all
    render :index
  end

  def show
    @band = Band.find_by(id: params[:id])
    render :show
  end

  def new
    @band = Band.new
    render :new
  end

  def create
    @band = Band.new(band_name: params[:band][:band_name])
    if @band.save
      redirect_to bands_url
    else
      render :new
    end
  end

end
