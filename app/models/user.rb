class User < ActiveRecord::Base

  validates :email, :password_digest, :presence => true
  validates :email, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true }

  attr_reader :password

  has_many(
  :notes,
  :class_name => 'Note',
  :foreign_key => :user_id,
  :primary_key => :id,
  dependent: :destroy
  )

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
   BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def generate_session_token## not finalized
    rand(10000).to_s
  end

  def reset_token
    self.session_token = self.generate_session_token
    self.save!
  end

  def self.find_by_credentials(email, password)
    user = User.find_by(email: email)
    if user && user.is_password?(password)
      return user
    else
      return nil
    end
  end


end
