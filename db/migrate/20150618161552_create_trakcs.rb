class CreateTrakcs < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :song_name, null: false
      t.integer :album_id

      t.timestamps
    end

    add_index :tracks, :album_id
  end
end
