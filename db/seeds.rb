

a7x = Band.create!(band_name: "a7x")
flaming_lips = Band.create!(band_name: "Flaming Lips")
pink_floyd = Band.create!(band_name: "Pink Floyd")

hail_to_the_king = Album.create!(title: "Hail to the King", band_id: a7x.id)
city_of_evil = Album.create!(title: "City of Evil", band_id: a7x.id)
nightmare = Album.create!(title: "Nightmare", band_id: a7x.id)

yoshimi = Album.create!(title: "Yoshimi Battles the Pink Robots", band_id: flaming_lips.id)
embryonic = Album.create!(title: "Embryonic", band_id: flaming_lips.id)
soft_bulletin = Album.create!(title: "The Soft Bulletin", band_id: flaming_lips.id)

the_wall = Album.create!(title: "The Wall", band_id: pink_floyd.id)
dark_side = Album.create!(title: "Dark Side of the Moon", band_id: pink_floyd.id)
wish = Album.create!(title: "Wish You Were Here", band_id: pink_floyd.id)


Track.create!(song_name: "Shepherd of Fire", album_id: hail_to_the_king.id)
Track.create!(song_name: "Doing Time", album_id: hail_to_the_king.id)
Track.create!(song_name: "Requiem", album_id: hail_to_the_king.id)
Track.create!(song_name: "Planets", album_id: hail_to_the_king.id)


Track.create!(song_name: "Bat Country", album_id: city_of_evil.id)
Track.create!(song_name: "Beast and the Harlot", album_id: city_of_evil.id)
Track.create!(song_name: "Burn it Down", album_id: city_of_evil.id)

Track.create!(song_name: "some song", album_id: nightmare.id)

Track.create!(song_name: "Fight Test", album_id: yoshimi.id)
Track.create!(song_name: "Yoshimi Battles the....", album_id: yoshimi.id)
Track.create!(song_name: "Another song", album_id: yoshimi.id)

Track.create!(song_name: "safsafsadf", album_id: embryonic.id)
Track.create!(song_name: "sdfafg", album_id: embryonic.id)

Track.create!(song_name: "something", album_id: soft_bulletin.id)
Track.create!(song_name: "something else", album_id: soft_bulletin.id)

Track.create!(song_name: "Wish You Were Here", album_id: wish.id)
Track.create!(song_name: "Welcome to the Machine", album_id: wish.id)
Track.create!(song_name: "blacblaabl", album_id: wish.id)
Track.create!(song_name: "Have a Cigar", album_id: wish.id)

Track.create!(song_name: "The Wall", album_id: the_wall.id)
Track.create!(song_name: "Another Brick in the Wall", album_id: the_wall.id)

Track.create!(song_name: "Time", album_id: dark_side.id)
Track.create!(song_name: "Breathe", album_id: dark_side.id)
Track.create!(song_name: "On the Run", album_id: dark_side.id)
Track.create!(song_name: "some other song", album_id: dark_side.id)
